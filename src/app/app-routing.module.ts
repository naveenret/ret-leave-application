import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin/admin.component';
import { UserComponent } from './components/user/user/user.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './components/login/login/login.module#LoginModule'
  },
  {
    path: 'signup',
    loadChildren: './components/signup/signup/signup.module#SignupModule'
  },
  {
    path: 'forget',
    loadChildren: './components/forget/forget/forget.module#ForgetModule'
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'addashboard',
        pathMatch: 'full'
      },
      {

        path: 'addashboard',
        loadChildren: './components/addashboard/addashboard/addashboard.module#AddashboardModule'
      }
    ]
  },
  {
    path: 'user',
    // loadChildren: './components/user/user/user.module#UserModule'
    component:UserComponent,
    children:[
      {
        path:'',
        redirectTo:'usrdashboard',
        pathMatch:'full'
      },
      {
        path:'usrdashboard',
        loadChildren:'./components/usrdashboard/usrdashboard/usrdashboard.module#UsrdashboardModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
