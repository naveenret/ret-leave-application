import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
public newForm;any;
  constructor(
    public router:Router,
    public fb:FormBuilder
  ) { 
    this.createNewForm();
  }
createNewForm(){
  this.newForm=this.fb.group({
    username:[null,Validators.required],
    dob:[null,Validators.required],
    email:[null,Validators.required],
    newPassword:[null,Validators.required],
    confrimPassword:[null,Validators.required],
  })
}
  ngOnInit() {
  }
signin(){
  this.router.navigate(['/login']);
}
submit(){
  console.log(this.newForm.value);
  if(this.newForm.valid){
    if(this.newForm.value.newPassword==this.newForm.value.confirmPassword){
      console.log(this.newForm.value);
      console.log("New account is created")
    }else{
      console.log("Password does not match");
    }
  }else{
    this.validateAllFormFields(this.newForm);
  }
}
validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}
}
