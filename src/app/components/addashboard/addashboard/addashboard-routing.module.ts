import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddashboardComponent } from './addashboard.component';


const routes: Routes = [
  {
    path:'',
    component:AddashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddashboardRoutingModule { }
