import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzTableModule } from 'ng-zorro-antd/table';

import { AddashboardRoutingModule } from './addashboard-routing.module';
import { AddashboardComponent } from './addashboard.component';


@NgModule({
  declarations: [AddashboardComponent],
  imports: [
    CommonModule,
    AddashboardRoutingModule,
    NzTableModule
  ]
})
export class AddashboardModule { }
