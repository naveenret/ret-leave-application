import { Component, OnInit, createPlatformFactory } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd';
declare var $ : any

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
public loginForm:any;
  constructor(
    public fb:FormBuilder,
    public router:Router,
    public notification: NzNotificationService
  ) {
    this.createForm();
   }
    createForm(){
      this.loginForm=this.fb.group(
        {
          username:[null,Validators.required],
          password:[null,Validators.required]
        }
      )
    }
    loginClick(type : string):void{
      console.log(this.loginForm.value);
      // $('#modalDemo').modal('show');
      if(this.loginForm.valid){
        if(this.loginForm.value.username=='admin'&& this.loginForm.value.password=='admin123'){
          this.notification.create(
            type,
            'Notification',
            'Login Successfull'
          );
          this.router.navigate(['/admin']);
        }else if(this.loginForm.value.username=='user'&& this.loginForm.value.password=='user123'){
          this.notification.create(
            type,
            'Notification',
            'Login Successfull'
          );
          this.router.navigate(['/user']);
        }else{
          this.router.navigate(['/login']);
        }
      }
      else{
        this.validateAllFormFields(this.loginForm);
      }
    }
    validateAllFormFields(formGroup: FormGroup) {
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
      });
    }
  ngOnInit() {
  }
forget(){
  this.router.navigate(['/forget'])
}
newAccount(){
  this.router.navigate(['/signup'])
}
}
