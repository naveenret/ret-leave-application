import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { ForgetRoutingModule } from './forget-routing.module';
import { ForgetComponent } from './forget.component';


@NgModule({
  declarations: [ForgetComponent],
  imports: [
    CommonModule,
    ForgetRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ForgetModule { }
