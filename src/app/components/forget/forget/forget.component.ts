import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent implements OnInit {
public forgetForm:any;
  constructor(
    public fb:FormBuilder
  ) {
    this.createForgetForm();
   }
createForgetForm(){
  this.forgetForm=this.fb.group({
    newemail:[null,Validators.required]
  })  
}
  ngOnInit() {
  }
  sendEmail(){
  console.log(this.forgetForm.value); 
  console.log("send button is clicked");
}
}
