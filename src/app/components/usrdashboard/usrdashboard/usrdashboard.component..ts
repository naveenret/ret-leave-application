import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/app/service/base.service';
import { FormBuilder, FormGroup } from '@angular/forms';


declare var $: any;
@Component({
  selector: 'app-usr-dashboard',
  templateUrl: './usrdashboard.component.html',
  styleUrls: ['./usrdashboard.component.scss']
})
export class UsrdashboardComponent implements OnInit {


  userForm: FormGroup;


  userList: any[] = [];
  constructor(
    public baseService: BaseService,
    public fb: FormBuilder
  ) {

    this.userForm = this.fb.group({
      username: null,
      id: null
    })

  }

  ngOnInit() {
    this.getUserlist();
  }

  getUserlist() {
    this.baseService.getUserArray().subscribe(res => {
      console.log(res);
      this.userList = res || [];
    })
  }

  getSingleUser(id) {

    this.baseService.getSingleUserById(id).subscribe(res => {
      console.log('res', res);

      this.userForm.patchValue({
        username: res.username,
        id: res.id
      });


      this.openModal();
    })

  }

  saveForm(value) {
    console.log(value);
    console.log(this.userForm.value);
    console.log(this.userForm.controls.username.value);
    console.log(this.userForm.controls.id.value);
    if(this.userForm.controls.id.value){
      this.baseService.updateUser(this.userForm.value).subscribe(res => {
        console.log('save form res', res);
        this.closeModal();
        this.getUserlist();
      }, error => {
        console.log('save form error', error);
      })
    }else{
      this.baseService.saveUser(this.userForm.value).subscribe(res => {
        console.log('save form res', res);
        this.closeModal();
        this.getUserlist();
      }, error => {
        console.log('save form error', error);
      })
    }

  }

  deleteUser(id){
   this.baseService.deleteUser(id).subscribe(res=>{
        this.getUserlist();
   })
  }


  openModal() {
    $('#modal').modal('show');
  }

  closeModal() {
    $('#modal').modal('hide');
    this.userForm.reset();

  }
}
