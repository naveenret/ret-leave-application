import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsrdashboardComponent } from './usrdashboard.component.';


const routes: Routes = [
  {
    path:'',
    component:UsrdashboardComponent  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsrdashboardRoutingModule { }
