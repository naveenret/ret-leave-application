import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzTableModule } from 'ng-zorro-antd/table';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { UsrdashboardRoutingModule } from './usrdashboard-routing.module';
import { UsrdashboardComponent } from './usrdashboard.component.';


@NgModule({
  declarations: [UsrdashboardComponent],
  imports: [
    CommonModule,
    UsrdashboardRoutingModule,
    NzTableModule,
    ReactiveFormsModule,
    FormsModule,
    ],
    exports:[
      UsrdashboardComponent
    ]
})
export class UsrdashboardModule { }
