import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    public http: HttpClient
  ) { }

  // getUserArray() : any{
  //   return of({
  //      data : [
  //            {
  //              id: '001',
  //              username: 'naveen'
  //            },
  //            {
  //              id: '002',
  //              username: 'kalai'
  //            },
  //            {
  //              id: '003',
  //              username: 'shivu'
  //            },
  //     ]  
  // })
  // }

  getUserArray(): any {

    return this.http.get('http://localhost:5000/users').pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  
  getSingleUserById(id): any {

    return this.http.get(`http://localhost:5000/users/${id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }



  saveUser(req: any): any {
    return this.http.post('http://localhost:5000/users', req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  updateUser(req) {
    return this.http.put(`http://localhost:5000/users/${req.id}`, req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  deleteUser(id) {
    return this.http.delete(`http://localhost:5000/users/${id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
}
